# eZ Platform Search Binary Extractor

It was possible to use third-party binaries to index binary files in eZ Publish. This functionality is missing in the latest eZ Platform versions. And this bundle provides it.

Also, it provides an example of a binary extractor for PDF files. Which uses [pdftotext](https://www.xpdfreader.com/download.html) third-party binary.

## Installation

1. Require `contextualcode/ezplatform-search-binary-extractor` via composer:
    ```bash
    composer require contextualcode/ezplatform-search-binary-extractor
    ```

2. Your `composer.json` must have this line:
    ```json
    "config": {
        "bin-dir": "bin/"
    }
    ```

## Usage

First of all, please double check if "Searchable" checkbox is checked for binary file field types that need to be searchable. 

After the bundle is installed, all the PDF files content will be indexed. And you would need to rebuild the search index by running:
```bash
php bin/console ezplatform:reindex
```

Also it is possible to build your own custom binary extractors. You just need to follow a few simple steps:

1. Create a new service which implements [`BinaryExtractor`](https://gitlab.com/contextualcode/ezplatform-search-binary-extractor/-/blob/2.0/src/FieldType/BinaryFile/BinaryExtractor.php). Please use [`BinaryExtractor\Pdf`](https://gitlab.com/contextualcode/ezplatform-search-binary-extractor/-/blob/2.0/src/FieldType/BinaryFile/BinaryExtractor/Pdf.php) as an example.

2. Tag your service with [`ezplatform.field_type.ezbinaryfile.binary_extractor` tag](https://gitlab.com/contextualcode/ezplatform-search-binary-extractor/-/blob/2.0/src/Resources/config/services.yml#L12).
