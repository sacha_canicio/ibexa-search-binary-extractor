<?php

declare(strict_types=1);

namespace ContextualCode\EzPlatformSearchBinaryExtractorBundle\FieldType\BinaryFile;

use eZ\Publish\Core\FieldType\BinaryFile\SearchField as Base;
use eZ\Publish\SPI\Persistence\Content\Field;
use eZ\Publish\SPI\Persistence\Content\Type\FieldDefinition;
use eZ\Publish\SPI\Search;

class SearchField extends Base
{
    /** @var BinaryExtractor[] */
    protected $binaryExtractors;

    public function __construct(iterable $binaryExtractors)
    {
        $this->binaryExtractors = $binaryExtractors;
    }

    public function getIndexData(Field $field, FieldDefinition $fieldDefinition): array
    {
        $indexData = parent::getIndexData($field, $fieldDefinition);
        $indexData[] = new Search\Field(
            'file_content',
            $this->extractFileContent($field),
            new Search\FieldType\FullTextField()
        );
        $indexData[] = new Search\Field(
            'file_content',
            $this->extractFileContent($field),
            new Search\FieldType\TextField()
        );

        return $indexData;
    }

    public function getIndexDefinition()
    {
        $indexDefinition = parent::getIndexDefinition();
        $indexDefinition['file_content'] = new Search\FieldType\TextField();

        return $indexDefinition;
    }

    protected function extractFileContent(Field $field): ?string
    {
        foreach ($this->binaryExtractors as $extractor) {
            if ($extractor instanceof BinaryExtractor && $extractor->supports($field)) {
                return $extractor->extract($field);
            }
        }

        return null;
    }
}
